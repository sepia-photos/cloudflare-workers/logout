# logout

Sepia's backend consists of serverless functions, MongoDB, and object storage. The object storage is hosted on Backblaze B2, and all requests to the API are handled by Cloudflare workers. When necessary, information is passed on to IBM cloud functions which can interface with the database.

This worker handles requests with the /logout endpoint and passes on information to the "logout" IBM cloud function.

## Deployment

Right now there is no deployment pipeline configuration setup. To deploy this yourself you either create a Cloudflare Worker and manually paste in the contents of index.js or configure [wrangler](https://developers.cloudflare.com/workers/cli-wrangler/install-update), Cloudflare's cli tool for workers.
